# Leren Charts

This repository contains the Helm-chart for [Leren](https://gitlab.com/pleio/leren) (and any future charts which are related to this project). 

## Installation
See [the Leren Chart's README](./leren/README.md) for installation instructions

## Chart Releases
Any changes in the chart directories (e.g. [leren](./leren/)) triggers a pipeline which publishes a chart package into this project's [package registry](https://gitlab.com/pleio/helm-charts/leren-charts/-/packages). 

## Development
Please create merge-requests for any additional features or bugfixes. Please do not forget to:
* bump the chart-version in the Chart.yaml (so `version`, `appversion` can stay as is)
* add any additional values in the [Leren chart README](./leren/README.md) and run [helm-docs](https://github.com/norwoodj/helm-docs/tree/master) to automatically generate the docs.