# leren

![Version: 0.1.3](https://img.shields.io/badge/Version-0.1.3-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for the Leren application stack to be run on Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/leren-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| hosts | list | `[{"host":"example.com"}]` | The domains used by clients to reach the Moodle web server. |
| imageRepository | string | `"registry.gitlab.com/pleio/leren"` | Default image repository for the Leren application. |
| imageTag | string | `""` | Leren application image tag to use. |
| mediaSharedStorageSize | string | `"20Gi"` | The size of the shared storage designated for media files, i.e. images and documents. |
| replicaCount | int | `1` | Amount of replicas used for the Leren web deployment. |
| sharedStorageClassName | string | `""` | The name of the Storage Class to be used for shared pod storage. Must be of access mode RWX. |
| useLetsEncryptCertificate | bool | `false` | A boolean value indicating whether to use an automatically obtained LetsEncrypt certificate for TLS. |
| wildcardTlsSecretName | string | `""` | The name of the K8S secret containing the key pair for the wildcard certificate used. To obtain a LetsEncrypt certificate, set to a name not associated to any secret to allow cert-manager to request a certificate. |
